import React from 'react';
import './menu.sass';

function Menu( props ) {
  function defaultAction() {
    console.info( 'default' );
  }

  const {
    position = 'bottom',
    closeWith = defaultAction
  } = props;

  return (
    <>
      <div className="closeArea" onClick={ () => closeWith() }>
      </div>
      <menu className={ position }>
        { props.children }
      </menu>
    </>
  );
}

export default Menu;
